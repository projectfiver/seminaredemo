"use strict";

const ld = require("lodash");

const mockData = require("./seminare.json");


// helper
function addUnique(array, element, registry) {
    if (!registry[element.id]) {
        array.push(element);
        registry[element.id] = true;
    }
}




export const state = () => ({
    records: [],
    typen: [],
    orte: [],
    referenten: []
});


export const mutations = {

    set(state, data) {

        // pre-sort
        const typen = [], orte = [], referenten = [];
        const typenReg = {}, orteReg = {}, referentenReg = {};

        for (let seminar of data) {
            addUnique(typen, seminar.Veranstaltung_Typ, typenReg);
            for (let event of seminar.Veranstaltung_Events) {
                addUnique(orte, event.Veranstaltung_Ort, orteReg);
                for (let referent of event.Veranstaltung_Referents) {
                    addUnique(referenten, referent, referentenReg);
                }
            }
        }

        // set
        Object.assign(state, {
            records: data,
            typen,
            orte,
            referenten
        });
    }

};


export const actions = {

    async get({commit}) {
        commit("set", mockData);
    },

};
