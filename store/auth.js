"use strict";

const emptyUser = {
    kennnummer: "",
    vorname: "",
    nachname: "",
    Primaeremail: {email: ""},
};

const mockUser = {
    kennnummer: "hans77",
    vorname: "Hans",
    nachname: "Mustermann",
    Primaeremail: {email: "hans.musti@hm.im"}
};



export const state = () => ({
    login: false,
    user: {...emptyUser},
});



export const mutations = {
    set(state, data) {
        return Object.assign(state, data);
    }
};


export const actions = {


    async get({commit}) {

        if (process.client) {
            const login = (localStorage.getItem('login') === "true");

            commit("set", {login, user: (login) ? mockUser : emptyUser });
        }
    },


    async login({commit}, {username, password}) {

        if (process.client) localStorage.setItem("login", "true");

        commit("set", { login: true, user: mockUser });
    },


    async logout({commit}) {

        if (process.client) localStorage.setItem("login", "false");

        commit("set", { login: false, user: emptyUser });
    }

};
