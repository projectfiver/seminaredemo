// import middlewares from '../../nodejs/frontend/kd'
export default {
    router: {
        // base: '/nuxt/'
    },
    css: [
        '@./assets/scss/main.scss',     // custom scss
    ],
    modules : [
        'bootstrap-vue/nuxt'            // custom build
    ],
    plugins: [
        { src: '~/plugins/vue-isotope', ssr: false },
    ],
    bootstrapVue: {
        bootstrapCSS: false,            // use custom scss
        bootstrapVueCSS: false,         // use custom scss
        config: {
            "breakpoints": ['xs', 'sm', 'md', 'lg' ]
        },
    }

    // serverMiddleware: middlewares
}